import './App.css';
import LockInterface from "./container/LockInterface/LockInterface";

const App = () => {
  return (
      <div className="App">
        <LockInterface/>
      </div>
  );
};

export default App;
