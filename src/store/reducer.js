const initialState = [];

const reducer = (state = initialState, action) =>{

    if (action.type === 'ADD') {
        return [...state, action.payload];
    }

    if (action.type === 'DELETE') {
        return [...state.filter(n=> n!== action.payload)];
    }

    return state;
}

export default reducer;