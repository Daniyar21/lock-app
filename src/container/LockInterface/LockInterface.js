import React, {useState} from 'react';
import './LockInterface.css';
import {useDispatch, useSelector} from "react-redux";
import {TRUE_ANSWER} from "../../true_answer";

const LockInterface = () => {
    const [answer, setAnswer] = useState(null);
    const dispatch = useDispatch();
    const lockNumbers = useSelector(state => state);
    const add = (e)=>dispatch({type: 'ADD', payload: e.target.innerHTML});

    const check = ()=>{
        if(lockNumbers.length>0){
        for (let i = 0; i < TRUE_ANSWER.length; i++) {
            for (let j = 0; j < lockNumbers.length; j++) {
                if (TRUE_ANSWER[i] === lockNumbers[j]) {
                   setAnswer(true);
                }else if(TRUE_ANSWER[i] !== lockNumbers[j]){
                    setAnswer(false);
                }
            }
        }
        }
    }
     let nameOfClass = 'lock-screen';
    if(answer){
        nameOfClass = 'lock-screen correct'
    }else if(answer === false){
        nameOfClass ='lock-screen incorrect'
    }


    let access = null;
    if(answer){
        access = 'Access Granted!'
    }else if(answer === false){
       access = 'Access Denied!'
    }

    const remove =() => dispatch({type:'DELETE', payload: lockNumbers[lockNumbers.length-1]});
    console.log(lockNumbers);
    return (
        <>
            <div className={nameOfClass}>
                {lockNumbers.map((number,i)=>(
                    <span key={i}>*</span>
                ))}
                <p>{access}</p>

            </div>
            <div className='lock-numbers'>
                <button onClick={event => add(event)}>1</button>
                <button onClick={event => add(event)}>2</button>
                <button onClick={event => add(event)}>3</button>
                <button onClick={event => add(event)}>4</button>
                <button onClick={event => add(event)}>5</button>
                <button onClick={event => add(event)}>6</button>
                <button onClick={event => add(event)}>7</button>
                <button onClick={event => add(event)}>8</button>
                <button onClick={event => add(event)}>9</button>
                <button onClick={event => add(event)}>0</button>
                <button onClick={remove}>&#8592;</button>
                <button onClick={check}>E</button>
            </div>
        </>
    );
};

export default LockInterface;